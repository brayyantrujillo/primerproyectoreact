import React from "react";
import styled from 'styled-components';
import Icon  from './Icon'
import Body  from './Body'
const Container = styled.div`
height:80px;
width:350px;
display: flex;
flex-direction:row;
margin-top:35px;
`

const App=(params)=> {
  return (
    <Container >
      <Icon ico={params.icon} colorIcon={params.colorIcon}/>
      <Body/>
    </Container>
  );
}

export default App;
