import React from "react";
import styled from 'styled-components';

const Container = styled.div`
width:75%;
padding-left: 25px;
display: flex;
flex-direction:column;
justify-content:center;

`
const Title = styled.p`
margin:0px;
color:#405382;
font-weigth: bold;
font-size:14px;
`
const Description = styled.p`
margin:0px;
margin-top:5px;
color:#828b92;
font-size:12px;
`
const Enlace = styled.a`
margin:0px;
margin-top:5px;
color:#3b8699;
font-size:12px;
`

const App=()=> {
  return (
    <Container >
     <Title>Paquete primiun</Title>
     <Description>Descubre nuevas funciones</Description>
     <Enlace>Haste premiun</Enlace>
    </Container>
  );
}

export default App;
